'use strict'

const reloj = document.querySelector('h1');

function format(num) {
    if (num < 10) {
        return '0' + num;
    }
    return num;
}

let seconds = 0;
let minutes = 0;
let hours = 0;
let days = 0;


function clock() {
    seconds ++;

    if(seconds / 60 ===1){
    seconds = 0;
    minutes++;
    
    if(minutes / 60 ===1){
    minutes = 0;
    hours++; 
    }

    if(hours / 24 ===1) {
    hours = 0;
    days++
    
    }
   
    }
   if(seconds % 5 === 0){ 
    reloj.textContent = `${format(days)}:${format(hours)}:${format(minutes)}:${format(seconds)}`;}
}
setInterval(clock, 1000);


clock();
