'use strict';

function convert(num, base) {
    const binary = String(num)
      .split('')
      .filter(i => i === '1' || i === '0');
  
  if(base === 10){
    return binary.reduce((acc, curr, index, arr) => {
      return acc + Number(curr) * Math.pow(2, arr.length - 1 - index);
    }, 0);}

    if(base === 2){ 
    const number = num;

    const result = number.toString(2);
    
    return result;
} 
}
  
  console.log(convert(10, 10));
  