'use strict';

const cells = document.querySelectorAll('td');
const table = document.querySelector('table');
const tr = document.querySelector('tr:last-of-type')
const button = document.querySelector('button')
function rn(max) {
    return Math.round(Math.random() * max);
}

function changeColor() {
    for (const cell of cells) {
        const color = `rgb(${rn(255)}, ${rn(255)}, ${rn(255)})`;
        cell.style.backgroundColor = color;
    }
}


setInterval(changeColor, 1000);

function add() {
const square = document.createElement('td');
tr.appendChild(square);
function changeColor2 (){
    const color = `rgb(${rn(255)}, ${rn(255)}, ${rn(255)})`;
    square.style.backgroundColor = color;
}
setInterval(changeColor2, 1000)
}




function handleButtonClick(e) {
    add(e.target);
   
}

if (button) {
    button.addEventListener('click', handleButtonClick);
}

